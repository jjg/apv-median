apv-median
----------

This is a C99 library for estimating medians from large datasets
in a streaming fashion. See the [documentation](src/doc/README.md) for
further details.

Compilation of this code needs the [Jannson][1] library for the
serialisation and deserialisation of the histogram structure to JSON.

To compile and install:

``` sh
./configure
make
sudo make install
```

See `./configure --help` for more configuration options.

> Note: this code was written when I worked at [Dressipi][2], who
> kindly released it under the MIT licence.  This is a fork of the
> original [repository][3] made to assure long-term maintenance of
> the library.

[1]: http://www.digip.org/jansson/
[2]: https://dressipi.com/
[3]: https://github.com/dressipi/apv-median
