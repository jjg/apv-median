default: all

RUBBISH = *~ tmp/*.log
CONFFILES = config.cache config.log config.status

all:
	$(MAKE) -C src all

install:
	$(MAKE) -C src install

uninstall:
	$(MAKE) -C src uninstall

test:
	$(MAKE) -C src test

clean:
	$(MAKE) -C src clean
	$(RM) $(RUBBISH)

spotless:
	$(MAKE) -C src spotless
	$(RM) $(CONFFILES)
	rm -rf autom4te.cache/
	rm -rf tmp/

.PHONY: test all clean spotless

VERSION = $(shell cat VERSION)

# this needs to be done once on first cloning the repo, only if you
# plan to develop the library: it symlinks files in .hooks (which
# is version controlled) to .git/hooks (which is not).

git-setup-hooks:
	bin/git-setup-hooks

git-release-commit:
	git add -u
	git commit -m $(VERSION)
	git push origin master

git-create-tag:
	bin/git-release-tag $(VERSION)

git-push-tag:
	git push origin v$(VERSION)

.PHONY: git-setup-hooks git-release-commit git-create-tag git-push-tag

update-autoconf:
	autoconf
	autoheader

update-assets:
	./configure
	$(MAKE) spotless

release:
	$(MAKE) update-autoconf
	$(MAKE) update-assets
	$(MAKE) git-release-commit
	$(MAKE) git-create-tag
	$(MAKE) git-push-tag

.PHONY: update-autoconf release

coverage-prepare:
	./configure --enable-coverage --enable-tests
	$(MAKE) all test

coverage-run:
	$(MAKE) -C src coverage

coverage-show:
	$(MAKE) -C src coverage-show

coverage-clean: spotless

.PHONY: coverage-prepare coverage-run coverage-show coverage-clean

update-coverage: coverage-prepare coverage-run coverage-clean

.PHONY: update-coverage
