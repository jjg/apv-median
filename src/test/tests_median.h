/*
  tests_median.h
  Copyright (c) Stylemania Ltd. 2018
*/

#ifndef TESTS_MEDIAN_H
#define TESTS_MEDIAN_H

#include <CUnit/CUnit.h>

extern CU_TestInfo tests_median[];

void test_median_small_permutations(void);
void test_median_small_equal(void);
void test_median_large_equal(void);
void test_median_uniform(void);
void test_median_half_gaussian(void);

#endif
