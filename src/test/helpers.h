/*
  helpers.h
  Copyright (c) Stylemania 2019
*/

#ifndef HELPERS_H
#define HELPERS_H

#include <stdlib.h>

int dbl_cmp(const void*, const void*);
double percentile_exact(size_t, const double*, double);
double median_exact(size_t, const double*);
double rand_uniform(void);
double rand_half_gaussian(void);

#endif
