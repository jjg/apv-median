/*
  tests_histogram.h
  Copyright (c) Stylemania Ltd. 2018
*/

#ifndef TESTS_HISTOGRAM_H
#define TESTS_HISTOGRAM_H

#include <CUnit/CUnit.h>

extern CU_TestInfo tests_histogram[];

void test_histogram_new_positive(void);
void test_histogram_new_zero(void);
void test_histogram_add_distinct(void);
void test_histogram_add_equal(void);
void test_histogram_post_init(void);
void test_histogram_add_duplicates(void);
void test_histogram_json_save(void);
void test_histogram_json_load_valid(void);
void test_histogram_json_load_nofile(void);
void test_histogram_json_load_maxnodes_absent(void);
void test_histogram_json_load_maxnodes_zero(void);
void test_histogram_json_load_nodes_absent(void);
void test_histogram_json_load_nodes_null(void);
void test_histogram_json_load_nodes_string(void);
void test_histogram_json_load_nodes_toobig(void);
void test_histogram_json_load_nodes_nomax(void);
void test_histogram_json_roundtrip(void);
void test_histogram_capacity_zero(void);
void test_histogram_capacity_empty(void);
void test_histogram_capacity_small(void);

#endif
