/*
  assert_errno.c

  Some helpwe methods for erron assertions in tests

  Copyright (c) Stylemania Ltd. 2018
*/

#include "assert_errno.h"

void clear_errno(void)
{
  errno = 0;
}

void assert_errno(int value)
{
  CU_ASSERT_EQUAL(errno, value);
  clear_errno();
}
