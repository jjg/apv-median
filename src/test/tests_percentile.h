/*
  tests_percentile.h
  Copyright (c) Stylemania Ltd. 2018
*/

#ifndef TESTS_PERCENTILE_H
#define TESTS_PERCENTILE_H

#include <CUnit/CUnit.h>

extern CU_TestInfo tests_percentile[];

void test_percentile_small_permutations(void);
void test_percentile_uniform(void);
void test_percentile_half_gaussian(void);
void test_percentile_non_decreasing(void);
void test_percentile_single_bin(void);
void test_percentile_domain_error(void);

#endif
