Icon
----

The file `apv-median-350.png` is derived from the [photograph][1]
by Jenn Deane (copyright Attribution 2.0 Generic (CC BY 2.0)), to
whom, many thanks.

[1]: https://www.flickr.com/photos/124254537@N02/15273019579/
