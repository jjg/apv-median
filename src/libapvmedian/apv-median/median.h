/*
  median.h
  Copyright (c) Stylemania Ltd. 2018
*/

#ifndef MEDIAN_H
#define MEDIAN_H

#include <apv-median/histogram.h>

int median(const histogram_t*, double*);

#endif
