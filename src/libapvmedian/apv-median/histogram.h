/*
  histogram.h
  Copyright (c) Stylemania Ltd. 2018
*/

#ifndef HISTOGRAM_H
#define HISTOGRAM_H

#include <stdlib.h>
#include <stdio.h>

#include <apv-median/bin.h>

typedef struct histogram_t histogram_t;

histogram_t* histogram_new(size_t);
void histogram_destroy(histogram_t*);
size_t histogram_num_bins(const histogram_t*);
bin_t* histogram_bins(const histogram_t*);
int histogram_add(histogram_t*, double);
int histogram_capacity(histogram_t*, double);
histogram_t* histogram_json_load(const char*);
histogram_t* histogram_json_load_stream(FILE*);
int histogram_json_save(const histogram_t*, const char*);
int histogram_json_save_stream(const histogram_t*, FILE*);

#endif
