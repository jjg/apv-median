/*
  percentile.h
  Copyright (c) Stylemania Ltd. 2019
*/

#ifndef PERCENTILE_H
#define PERCENTILE_H

#include <apv-median/histogram.h>

int percentile(const histogram_t*, double, double*);

#endif
