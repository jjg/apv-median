/*
  node.h
  Copyright (c) Stylemania Ltd. 2018
*/

#ifndef NODE_H
#define NODE_H

#include <apv-median/bin.h>

typedef struct node_t node_t;

struct node_t
{
  bin_t bin;
  node_t *next;
};

node_t* node_new(double, node_t*);
void node_destroy(node_t*);

#endif
