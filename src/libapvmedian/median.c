/*
  median.c
  Copyright (c) Stylemania Ltd. 2018
*/

#include <apv-median/median.h>
#include <apv-median/percentile.h>

int median(const histogram_t *hist, double *value)
{
  return percentile(hist, 50.0, value);
}
