/*
  apv-median.h
  Copyright (c) J.J. Green 2023
*/

#ifndef APV_MEDIAN_H
#define APV_MEDIAN_H

#include <apv-median/bin.h>
#include <apv-median/histogram.h>
#include <apv-median/median.h>
#include <apv-median/node.h>
#include <apv-median/percentile.h>

#endif
